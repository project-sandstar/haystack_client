///
/// Copyright (c) 2023, BAS Services & Graphics, LLC.
/// Licensed under the Academic Free License version 3.0
///

library haystack.src.client;

import 'dart:async';

import 'package:collection/collection.dart' show IterableExtension;
import 'package:haystack/haystack.dart';
import 'package:haystack_client/src/auth/auth.dart';
import 'package:http/http.dart' as http;


/// Client to connect to any SkySpark server viw HTTP.
///
/// You can use it ro connect to any haystack server too, but haystack doesn't
/// support `eval` and `evalAll` api.
///
/// See [Project Haystack](http://project-haystack.org/doc/Rest).
class HSkySparkClient extends SkysparkProject {
  factory HSkySparkClient(String uri, String user, String pass) {
    if (!uri.endsWith("/")) uri = uri + "/";

    // sanity check arguments
    if (user.isEmpty) {
      throw ArgumentError("user cannot be empty string");
    }

    return HSkySparkClient._(uri, user, pass, null);
  }

  factory HSkySparkClient.web(String uri, String attestKey) {
    if (!uri.endsWith("/")) uri = uri + "/";
    assert(attestKey.isNotEmpty,
      "In order to send requests from browser you have to use attest key in request header.");

    return HSkySparkClient._(uri, "", "", attestKey);
  }

  HSkySparkClient._(this.uri, this._user, this._pass, this._attestKey) {
    this._auth = AuthClientContext(uri + "about", _user, _pass);
  }

  /// Creates new [HSkySparkClient] based on existing one with uri for other project.
  ///
  /// Works only for different projects within one Skyspark. They have same
  /// auth token, no need to get new.
  HSkySparkClient copyWith(String newUri) {
    var newClient = HSkySparkClient(newUri, _user, _pass);
    newClient._auth = _auth;
    return newClient;
  }

//////////////////////////////////////////////////////////////////////////
// State
//////////////////////////////////////////////////////////////////////////

  /// Base URI for connection such as "http://host/api/demo/".
  ///
  /// This string always ends with slash.
  final String uri;

  /// Timeout in milliseconds for opening the HTTP socket.
  int connectTimeout = 60 * 1000;

  /// Timeout in milliseconds for reading from the HTTP socket.
  int readTimeout = 60 * 1000;

//////////////////////////////////////////////////////////////////////////
// Operations
//////////////////////////////////////////////////////////////////////////

  /// Authenticate the client and return this.
  Future<HSkySparkClient> open() async {
    _auth.connectTimeout = this.connectTimeout;
    _auth.readTimeout = this.readTimeout;
    await _auth.open();
    return Future(() => this);
  }

  /// Call "about" to query summary info.
  @override
  Future<HDict> about() => call("about", HGrid.EMPTY).then((HGrid res) => res.row(0));

  /// Call "ops" to query which operations are supported by server.
  Future<HGrid> ops() {
    return call("ops", HGrid.EMPTY);
  }

  /// Call "formats" to query which MIME formats are available.
  Future<HGrid> formats() {
    return call("formats", HGrid.EMPTY);
  }

  @override
  Future<HGrid> commit(Iterable<HDict> diffs, CommitOperator operator, {bool force = false}) {
    var meta = HDictBuilder().add('commit', commitToString(operator));
    if (force) {
      meta.add('force');
    }

    var grid = HGridBuilder.dictsToGrid(meta.toDict(), diffs.toList());
    return call("commit", grid);
  }

//////////////////////////////////////////////////////////////////////////
// Reads
//////////////////////////////////////////////////////////////////////////

  @override
  Future<HDict?> onReadById(HRef id) {
    return readByIds([id], checked: false).then((HGrid res) {
      if (res.isEmpty) return null;
      HDict rec = res.row(0);
      if (rec.missing("id")) return null;
      return rec;
    });
  }

  @override
  Future<HGrid> onReadByIds(List<HRef> ids) {
    var b = HGridBuilder();
    b.addCol("id");

    for (int i = 0; i < ids.length; ++i) {
      b.addRow([ids[i]]);
    }

    return call("read", b.toGrid());
  }

  @override
  Future<HGrid> onReadAll(String filter, int limit) {
    var b = HGridBuilder();
    b.addCol("filter");
    b.addCol("limit");
    b.addRow([HStr(filter), HNum(limit)]);

    return call("read", b.toGrid());
  }

//////////////////////////////////////////////////////////////////////////
// Evals
//////////////////////////////////////////////////////////////////////////

  /// Call "eval" operation to evaluate a vendor specific
  /// expression on the server:
  ///   - SkySpark: any Axon expression
  ///
  /// Raise [CallErrException] if the server raises an exception.
  @override
  Future<HGrid> eval(String expr) {
    var b = HGridBuilder();
    b.addCol("expr");
    b.addRow([HStr(expr)]);

    return call("eval", b.toGrid());
  }

  /// Convenience for "evalAll(HGrid, checked)".
  Future<List<HGrid>> evalAllStrings(List<String> exprs, {bool checked = true}) {
    var b = HGridBuilder();
    b.addCol("expr");

    for (int i = 0; i < exprs.length; ++i) {
      b.addRow([HStr(exprs[i])]);
    }

    return evalAll(b.toGrid(), checked: checked);
  }

  /// Call "evalAll" operation to evaluate a batch of vendor specific
  /// expressions on the server.
  ///
  /// See "eval" method for list of vendor
  /// expression formats.  The request grid must specify an "expr" column.
  /// A separate grid is returned for each row in the request.  If checked
  /// is false, then this call does *not* automatically check for error
  /// grids.  Client code must individual check each grid for partial
  /// failures using "Grid.isErr".  If checked is true and one of the
  /// requests failed, then raise CallErrException for first failure.
  Future<List<HGrid>> evalAll(HGrid req, {bool checked = true}) {
    String reqStr = HZincWriter.gridToString(req);

    return _postString("evalAll", reqStr).then((String resStr) {
      List<HGrid> res = HZincReader(resStr).readGrids();

      if (checked) {
        var errorGrid = res.firstWhereOrNull((g) => g.isErr);

        if (errorGrid != null) {
          throw CallErrError(errorGrid);
        }
      }

      return res;
    });
  }

//////////////////////////////////////////////////////////////////////////
// Watches
//////////////////////////////////////////////////////////////////////////

  /// Create a new watch with an empty subscriber list.
  ///
  /// The dis string is a debug string to keep track of who
  /// created the watch.
  @override
  HWatch watchOpen(String dis, HNum? lease) {
    return HClientWatch(this, dis, lease);
  }

  /// List the open watches associated with this [HSkySparkClient].
  ///
  /// This list does *not* contain a watch until it has been successfully
  /// subscribed and assigned an identifier by the server.
  @override
  List<HWatch> watches() => _watches.values.toList(growable: false);

  /// Lookup a watch by its unique identifier associated with this [HSkySparkClient].
  ///
  /// If not found return null or raise UnknownWatchException based on
  /// checked flag.
  @override
  HWatch? watch(String id, {bool checked = true}) {
    HWatch? w = _watches[id];
    if (w != null) return w;
    if (checked) throw UnknownWatchError(id);
    return null;
  }

  Future<HGrid> watchSub(HClientWatch w, List<HRef> ids, {bool checked = true}) {
    if (ids.isEmpty) throw ArgumentError("ids are empty");
    if (w.isClosed) throw StateError("watch is closed");

    // grid meta
    var b = HGridBuilder();
    if (w.id != null) b.meta.add("watchId", w.id);
    b.meta.add("lease", w._desiredLease);
    b.meta.add("watchDis", w.dis);

    // grid rows
    b.addCol("id");
    for (int i = 0; i < ids.length; ++i) {
      b.addRow([ids[i]]);
    }

    // make request
    return call("watchSub", b.toGrid()).then((HGrid res) {
      // make sure watch is stored with its watch id
      if (w.id == null) {
        w._id = res.meta.getStr("watchId");
        w._lease = res.meta.get("lease") as HNum?;
        _watches[w.id!] = w;
      }

      // if checked, then check it
      if (checked) {
        if (res.numRows != ids.length && ids.isNotEmpty) throw UnknownRecError(id: ids[0]);

        for (int i = 0; i < res.numRows; ++i) {
          if (res.row(i).missing("id")) throw UnknownRecError(id: ids[i]);
        }
      }

      return res;
    }, onError: (Object e) {
      if (e is CallErrError) {
        // any server side error is considered close
        watchClose(w, send: false);
      }

      throw e;
    });
  }

  Future<HGrid> watchUnsub(HClientWatch w, List<HRef> ids) {
    if (ids.isEmpty) throw ArgumentError("ids are empty");
    if (w.id == null) throw StateError("nothing subscribed yet");
    if (w.isClosed) throw StateError("watch is closed");

    // grid meta
    var b = HGridBuilder();
    b.meta.add("watchId", w.id);

    // grid rows
    b.addCol("id");

    for (int i = 0; i < ids.length; ++i) {
      b.addRow([ids[i]]);
    }

    // make request
    return call("watchUnsub", b.toGrid());
  }

  Future<HGrid> watchPoll(HClientWatch w, {bool refresh = false}) {
    if (w.id == null) throw StateError("nothing subscribed yet");
    if (w.isClosed) throw StateError("watch is closed");

    // grid meta
    var b = HGridBuilder();
    b.meta.add("watchId", w.id);
    b.addCol("empty");

    if (refresh) {
      b.meta.add("refresh");
    }

    // make request
    return call("watchPoll", b.toGrid()).catchError((Object e) {
      if (e is CallErrError) {
        // any server side error is considered close
        watchClose(w, send: false);
      }
      throw e;
    });
  }

  Future<Object> watchClose(HClientWatch w, {bool send = true}) {
    // mark flag on watch itself, short circuit if already closed
    if (w.isClosed) {
      return Future.value();
    }

    w._closed = true;

    // remove it from my lookup table
    if (w.id != null) {
      _watches.remove(w.id);
    }

    // optionally send close message to server
    if (send) {
      HGridBuilder b = HGridBuilder();
      b.meta.add("watchId", w.id).add("close");
      b.addCol("id");

      // Ignoring any error while unsub.
      return call("watchUnsub", b.toGrid()).catchError((Object e) {});
    } else {
      return Future.value();
    }
  }

//////////////////////////////////////////////////////////////////////////
// PointWrite
//////////////////////////////////////////////////////////////////////////

  /// Write to a given level of a writable point, and return the current status
  /// of a writable point's priority array (see pointWriteArray()).
  ///
  /// [id] is identifier of writable point.
  /// [level] from 1-17 for level to write.
  /// [val] to write or null to auto the level.
  /// [who] is optional username performing the write, otherwise user dis is used.
  /// [dur] should be with unit if setting level 8.
  Future<HGrid> pointWrite(HRef id, int level, String who, HVal val, HNum dur) {
    var b = HGridBuilder();
    b.addCol("id");
    b.addCol("level");
    b.addCol("who");
    b.addCol("val");
    b.addCol("duration");

    b.addRow([id, HNum(level), HStr(who), val, dur]);

    return call("pointWrite", b.toGrid());
  }

  /// Return the current status of a point's priority array.
  ///
  /// The result is returned grid with following columns:
  /// <ul>
  ///   <li>level: number from 1 - 17 (17 is default)
  ///   <li>levelDis: human description of level
  ///   <li>val: current value at level or null
  ///   <li>who: who last controlled the value at this level
  /// </ul>
  Future<HGrid> pointWriteArray(HRef id) {
    var b = HGridBuilder();
    b.addCol("id");
    b.addRow([id]);

    return call("pointWrite", b.toGrid());
  }

//////////////////////////////////////////////////////////////////////////
// History
//////////////////////////////////////////////////////////////////////////

  /// Read history time-series data for given record and time range.
  ///
  /// The items returned are exclusive of start time and inclusive of end time.
  /// Raise exception if id does not map to a record with the required tags
  /// "his" or "tz".  The range may be either a String or a HDateTimeRange.
  /// If HTimeDateRange is passed then must match the timezone configured on
  /// the history record.  Otherwise if a String is passed, it is resolved
  /// relative to the history record's timezone.
  Future<HGrid> hisRead(HRef id, Object range) {
    var b = HGridBuilder();
    b.addCol("id");
    b.addCol("range");
    b.addRow([id, HStr(range.toString())]);

    return call("hisRead", b.toGrid());
  }

  /// Write a set of history time-series data to the given point record.
  ///
  /// The record must already be defined and must be properly tagged as
  /// a historized point. The timestamp timezone must exactly match the
  /// point's configured "tz" tag. If duplicate or out-of-order items are
  /// inserted then they must be gracefully merged.
  Future<HGrid> hisWrite(HRef id, List<HHisItem> items) {
    HDict meta = HDictBuilder().add("id", id).toDict();
    HGrid req = HGridBuilder.hisItemsToGrid(meta, items);

    return call("hisWrite", req);
  }

//////////////////////////////////////////////////////////////////////////
// Actions
//////////////////////////////////////////////////////////////////////////

  /// Invoke a remote action using the "invokeAction" REST operation.
  Future<HGrid> invokeAction(HRef id, String action, HDict args) {
    HDict meta = HDictBuilder().add("id", id).add("action", action).toDict();
    HGrid req = HGridBuilder.dictsToGrid(meta, [args]);

    return call("invokeAction", req);
  }

//////////////////////////////////////////////////////////////////////////
// Call
//////////////////////////////////////////////////////////////////////////

  /// Make a call to the given operation.
  ///
  /// The request grid is posted to the URI "this.uri+op" and the response
  /// is parsed as a grid. Raise CallNetworkException if there is a
  /// communication I/O error. Raise CallErrException if there is a server
  /// side error and an error grid is returned.
  Future<HGrid> call(String op, HGrid req) {
    return _postGrid(op, req).then((HGrid res) {
      if (res.isErr) throw CallErrError(res);
      return res;
    });
  }

  Future<HGrid> _postGrid(String op, HGrid req) {
    String reqStr = HZincWriter.gridToString(req);
    return _postString(op, reqStr).then((String resStr) => HZincReader(resStr).readGrid());
  }

  Future<String> _postString(String op, String req) {
    Map<String, String> reqHeaders = Map<String, String>();

    if (_auth.headers.containsKey("Authorization")) {
      reqHeaders["Authorization"] = _auth.headers["Authorization"]!;
    }

    if (_attestKey != null) {
      reqHeaders["SkyArc-Attest-Key"] = _attestKey!;
    }

    reqHeaders["Content-Type"] = "text/zinc; charset=utf-8";

    return http.post(Uri.parse(uri + op), headers: reqHeaders, body: req).then((http.Response response) {
      return response.body;
    });
  }

//////////////////////////////////////////////////////////////////////////
// Fields
//////////////////////////////////////////////////////////////////////////

  final String _user;
  final String _pass;

  // Attest key used in request headers in case they are sent from browser.
  // In this case you don't need [authHeaders].
  // Can be null in case client is created not for browser.
  final String? _attestKey;
  late AuthClientContext _auth;
  Map<String, String> get authHeaders => _auth.headers;
  set authHeaders(Map<String, String> authHeaders) => _auth.headers = authHeaders;

  Map<String, HWatch> _watches = Map<String, HWatch>();
}


class HClientWatch extends HWatch {
  @override
  String? get id => _id;

  @override
  String get dis => _dis;

  @override
  bool get isOpen => !_closed;

  bool get isClosed => _closed;


  HClientWatch(this._client, this._dis, this._desiredLease);


  @override
  HNum? lease() => _lease;

  @override
  Future<HGrid> sub(List<HRef> ids, {bool checked = true}) => _client.watchSub(this, ids, checked: checked);

  @override
  Future<HGrid> unsub(List<HRef> ids) => _client.watchUnsub(this, ids);

  @override
  Future<HGrid> pollChanges() => _client.watchPoll(this, refresh: false);

  @override
  Future<HGrid> pollRefresh() => _client.watchPoll(this, refresh: true);

  @override
  Future close() => _client.watchClose(this, send: true);


  final HSkySparkClient _client;
  final String _dis;
  final HNum? _desiredLease;
  HNum? _lease;
  String? _id;
  bool _closed = false;
}
