///
/// Copyright (c) 2019, BAS Services & Graphics, LLC.
/// Licensed under the Academic Free License version 3.0
///

library haystack.src.auth;

import 'dart:async';
import 'dart:convert';

import 'package:haystack_client/src/auth/auth_exception.dart';
import 'package:haystack_client/src/auth/auth_msg.dart';
import 'package:haystack_client/src/auth/auth_scheme.dart';
import 'package:http/http.dart' as http;


class AuthClientContext {
//////////////////////////////////////////////////////////////////////////
// Construction
//////////////////////////////////////////////////////////////////////////

  AuthClientContext(this.uri, this.user, this.pass);

//////////////////////////////////////////////////////////////////////////
// State
//////////////////////////////////////////////////////////////////////////

  /// URI used to open the connection.
  final String uri;

  /// Username used to open the connection.
  final String user;

  /// Plaintext password for authentication.
  final String pass;

  /// User agent string.
  /// FIXME Can we change it to "HaystackDart"?
  final String userAgent = "HaystackJava";

  /// Headers we wish to use for authentication requests
  Map<String, String> headers = Map<String, String>();

  /// Stash allows you to store state between messages while authenticating with the server.
  final Map<String, String> stash = Map<String, String>();


  bool isAuthenticated() {
    return this.authenticated;
  }

/////////////////////////////////////////////////////////////////////////
// Fields
//////////////////////////////////////////////////////////////////////////

  /// Set true after successful authentication.
  bool authenticated = false;

  /// Timeout in milliseconds for opening the HTTP socket.
  int connectTimeout = 60 * 1000;

  /// Timeout in milliseconds for reading from the HTTP socket.
  int readTimeout = 60 * 1000;

//////////////////////////////////////////////////////////////////////////
// Open
//////////////////////////////////////////////////////////////////////////

  Future<AuthClientContext> open() async {
    try {
      // send initial hello message
      http.Response helloResp = await sendHello();
//try { dumpRes(helloResp, false); } catch (Exception e) { e.printStackTrace(); }

      // first try standard authentication va RFC 7235 process
      bool isOpened = await _openStd(helloResp);
      if (isOpened) return success();

      // check if we have a 200
      if (helloResp.statusCode == 200) return success();

      String content = helloResp.body;
      List<AuthScheme> schemes = AuthScheme.list();
      for (int i = 0; i < schemes.length; ++i) {
        if (schemes[i].onClientNonStd(this, helloResp, content)) { //always false
          return success();
        }
      }

      // give up
      int resCode = helloResp.statusCode;
      String resServer = helloResp.headers["Server"]!;
      if (resCode / 100 >= 4) {
        throw Exception(
            "HTTP error code: " + resCode.toString()); // 4xx or 5xx
      }

      throw AuthException("No suitable auth scheme for: " +
          resCode.toString() +
          " " +
          resServer);
    } catch (e) {
      throw AuthException("authenticate failed: ${e.toString()}");
    } finally {
      // this.pass = null;
      this.stash.clear();
    }
  }

  Future<http.Response> sendHello() {
    // hello message
    Map<String, String> params = Map<String, String>();
    params["username"] = Base64Codec().encode(this.user.codeUnits);
    AuthMsg hello = AuthMsg("hello", params);
    return _getAuth(hello);
  }

  AuthClientContext success() {
    this.authenticated = true;
    return this;
  }

  /// Attempt standard authentication via Haystack/RFC 7235
  ///
  /// @param resp The response to the hello message
  /// @return true if haystack authentciation was used, false if the
  /// server does not appear to implement RFC 7235.
  Future<bool> _openStd(http.Response resp) async {
    // must be 401 challenge with WWW-Authenticate header
    if (resp.statusCode != 401) return false;
    String wwwAuth = _resHeader(resp, "WWW-Authenticate");

    // don't use this mechanism for Basic which we
    // handle as a non-standard scheme because the headers
    // don't fit nicely into our restricted AuthMsg format
    if (wwwAuth.toLowerCase().startsWith("basic")) return false;

    // process res/req messages until we have 200 or non-401 failure
    late AuthScheme scheme;
    for (int loopCount = 0; true; ++loopCount) {
      // sanity check that we don't loop too many times
      if (loopCount > 5) throw AuthException("Loop count exceeded");

      // parse the WWW-Auth header and use the first scheme
      String header = _resHeader(resp, "WWW-Authenticate");
      List<AuthMsg> resMsgs = AuthMsg.listFromStr(header);
      AuthMsg resMsg = resMsgs[0];
      scheme = AuthScheme.find(resMsg.scheme)!;

      // let scheme handle message
      AuthMsg reqMsg = scheme.onClient(this, resMsg);

      // send request back to the server
      resp = await _getAuth(reqMsg);
      //try { dumpRes(resp, false); } catch (Exception e) { e.printStackTrace(); }

      // 200 means we are done, 401 means keep looping,
      // consider anything else a failure
      if (resp.statusCode == 200) {
        break;
      } else if (resp.statusCode == 401) {
        continue;
      } else {
        throw AuthException(
            "" + resp.statusCode.toString() + " " + resp.statusCode.toString());
      }
    }

    // init the bearer token
    String authInfo = _resHeader(resp, "Authentication-Info");
    AuthMsg authInfoMsg = AuthMsg.fromStr("bearer " + authInfo)!;

    // callback to scheme for client success
    scheme.onClientSuccess(this, authInfoMsg);

    // only keep authToken parameter for Authorization header
    Map<String, String?> map = Map<String, String?>();
    map["authToken"] = authInfoMsg.param("authToken");
    authInfoMsg = AuthMsg("bearer", map);
    this.headers["Authorization"] = authInfoMsg.toString();

    // we did it!
    return true;
  }

////////////////////////////////////////////////////////////////
// HTTP Messaging
////////////////////////////////////////////////////////////////

  Future<http.Response> _getAuth(AuthMsg msg) {
    Map<String, String> newHeaders = Map<String, String>();
    headers.forEach((name, val) => newHeaders[name] = val);

    newHeaders["User-Agent"] = this.userAgent;
    newHeaders["Authorization"] = msg.toString();

    return http.get(Uri.parse(uri), headers: newHeaders).then((http.Response response) {
      return response;
    });
  }

  String _resHeader(http.Response response, String name) {
    //maybe different cases so first find appropriate header
    String header = response.headers.keys
        .where((headName) => headName.toLowerCase() == name.toLowerCase())
        .first;

    String? val = response.headers[header];
    if (val == null) {
      throw AuthException("Missing required header: " + name);
    }

    return val;
  }
}
