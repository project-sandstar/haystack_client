///
/// Copyright (c) 2023, BAS Services & Graphics, LLC.
/// Licensed under the Academic Free License version 3.0
///

import 'package:haystack/haystack.dart';


/// AuthException is thrown by the authentication framework if an error occurs while trying
/// to authenticate a user.
class AuthException extends CallError {
  AuthException(String s) : super(s);
}
