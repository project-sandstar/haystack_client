///
/// Copyright (c) 2023, BAS Services & Graphics, LLC.
/// Licensed under the Academic Free License version 3.0
///

import 'package:haystack/haystack.dart';
import 'package:haystack_client/src/util/web_util.dart';


/// AuthMsg models a scheme name and set of parameters according
/// to <a href="https://tools.ietf.org/html/rfc7235">RFC 7235</a>.
///
/// To simplify parsing, we restrict the grammar to be auth-param and token (the
/// token68 and quoted-string productions are not allowed).
class AuthMsg {
//////////////////////////////////////////////////////////////////////////
// Construction
//////////////////////////////////////////////////////////////////////////

  /// Parses a list of AuthSchemes such as a list of 'challenge' productions
  /// for the WWW-Authentication header per RFC 7235.
  static List<AuthMsg> listFromStr(String s) {
    List<String> toks = splitList(s);
    List<AuthMsg> arr = <AuthMsg>[];

    for (int i = 0; i < toks.length; ++i) {
      final msg = AuthMsg.fromStr(toks[i]);
      if (msg != null) arr.add(msg);
    }

    return arr;
  }

  static AuthMsg? fromStr(String s, {bool checked = true}) {
    try {
      return _decode(s);
    } catch (e) {
      if (checked) throw ParseError(e.toString());
      return null;
    }
  }

  AuthMsg(this.scheme, this.params) {
    this._toStr = encode(this.scheme, this.params);
  }

////////////////////////////////////////////////////////////////////////////
//// Identity
////////////////////////////////////////////////////////////////////////////

  /// Scheme name normalized to lowercase.
  final String scheme;

  /// Parameters for scheme (read-only).
  final Map params;

  late String _toStr;

  //  public boolean equals(Object o)
//  {
//    if (this == o) return true;
//    if (o == null || getClass() != o.getClass()) return false;
//
//    AuthMsg authMsg = (AuthMsg)o;
//
//    if (!scheme.equals(authMsg.scheme)) return false;
//    return params.equals(authMsg.params);
//
//  }
//
//  public int hashCode()
//  {
//    int result = scheme.hashCode();
//    Iterator iter = params.keySet().iterator();
//    while (iter.hasNext())
//    {
//      String key = (String)iter.next();
//      result = 31 * result + key.toLowerCase().hashCode();
//      result = 31 * result + params.get(key).hashCode();
//    }
//    return result;
//  }

  @override
  String toString() {
    return _toStr;
  }

//
//  /**
//   * Convenience for {@link #param(String, boolean) param(name, true)}
//   */
//  public String param(String name) { return param(name, true); }
//
  /// Looks up a parameter by name. If not found and checked, throw an Exception,
  /// otherwise return null.
  String? param(String name, {bool checked = true}) {
    String? val = params[name];
    if (val != null) return val;
    if (checked) throw  Exception("parameter not found: " + name);
    return null;
  }

////////////////////////////////////////////////////////////////////////////
//// Encoding
////////////////////////////////////////////////////////////////////////////

  static List<String> splitList(String s) {
    // find break indices (start of each challenge production)
    List<String> toks = s.split(",");
    for (int i = 0; i < toks.length; ++i) {
      toks[i] = toks[i].trim();
    }

    List<int> breaks = <int>[];
    for (int i = 0; i < toks.length; ++i) {
      String tok = toks[i];
      int sp = tok.indexOf(' ');
      String name = (sp < 0 ? tok : tok.substring(0, sp)).trim();
      if (WebUtil.isToken(name) && i > 0) {
        breaks.add(i);
      }
    }

    // rejoin tokens into challenge strings
    List<String> acc = <String>[];

    int start = 0;
    String sb = "";

    for (int i = 0; i < breaks.length; i++) {
      int end = breaks[i];

      for (int k = start; k < end; ++k) {
        if (k > start) {
          sb = sb + ",";
        }
        sb = sb + toks[k];
      }

      acc.add(sb);
      start = end;
    }

    sb = "";
    for (int i = start; i < toks.length; ++i) {
      if (i > start) {
        sb = sb + ',';
      }
      sb = sb + toks[i];
    }
    acc.add(sb);
    return acc;
  }

  static AuthMsg _decode(String s) {
    int sp = s.indexOf(' ');
    String scheme = s;
    Map<String, String> params = Map<String, String>();

    if (sp >= 0) {
      scheme = s.substring(0, sp);
      List<String> paramParts = s.substring(sp + 1).split(",");

      for (int i = 0; i < paramParts.length; ++i) {
        String part = paramParts[i].trim();
        int eq = part.indexOf('=');
        if (eq < 0) throw ParseError("Invalid auth-param: " + part);
        params[part.substring(0, eq).trim()] = part.substring(eq + 1).trim();
      }
    }
    return AuthMsg(scheme, params);
  }

  static String encode(String scheme, Map params) {
    List<String> keys = List.from(params.keys);
    keys.sort((a, b) => a.toUpperCase().compareTo(b.toUpperCase()));

    bool first = true;
    String sb = _addToken(scheme);
    keys.forEach((key) {
      String val = params[key];

      if (first) {
        first = false;
      } else {
        sb = sb + ",";
      }

      sb = sb + " ";
      sb = sb + _addToken(key);
      sb = sb + "=";
      sb = sb + _addToken(val);
    });

    return sb;
  }

  static String _addToken(String val) {
    //base64 encryption gives "=" at the end
    if (val.endsWith("=")) {
      val = val.replaceAll("=", "");
    }

    String sb = "";
    for (int i = 0; i < val.length; ++i) {
      int cp = val.codeUnitAt(i);
      if (WebUtil.isTokenChar(cp)) {
        sb = sb + val[i];
      } else {
        throw Exception("Invalid char '" + val[i] + "' in " + val);
      }
    }

    return sb;
  }
}
