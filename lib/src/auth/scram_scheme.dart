///
/// Copyright (c) 2023, BAS Services & Graphics, LLC.
/// Licensed under the Academic Free License version 3.0
///

import 'dart:convert';
import 'dart:math';

import 'package:crypto/crypto.dart';
import 'package:haystack_client/src/auth/auth.dart';
import 'package:haystack_client/src/auth/auth_exception.dart';
import 'package:haystack_client/src/auth/auth_msg.dart';
import 'package:haystack_client/src/auth/auth_scheme.dart';
import 'package:haystack_client/src/util/crypto_util.dart';


/// ScramScheme implements the salted challenge response authentication
/// mechanism as defined in <a href="https://tools.ietf.org/html/rfc5802">RFC 5802</a>.
class ScramScheme extends AuthScheme {
  ScramScheme() : super("scram");

  @override
  AuthMsg onClient(AuthClientContext cx, AuthMsg msg) {
    return msg.param("data", checked: false) == null
        ? _firstMsg(cx, msg)
        : _finalMsg(cx, msg);
  }

  AuthMsg _firstMsg(AuthClientContext cx, AuthMsg msg) {
    // construct client-first-message
    String c_nonce = _genNonce();
    String c1_bare = "n=" + cx.user + ",r=" + c_nonce;
    String c1_msg = _gs2_header + c1_bare;

    // stash for final msg
    cx.stash["c_nonce"] = c_nonce;
    cx.stash["c1_bare"] = c1_bare;

    // build auth msg
    Map<String, String> params = Map<String, String>();

    //somehow after encoding string have "=" on the end, doesn't have in java lib
    params["data"] = Base64Codec.urlSafe().encode(c1_msg.codeUnits)
        .replaceAll("=", "");

    return AuthMsg(name, _injectHandshakeToken(msg, params));
  }

  AuthMsg _finalMsg(AuthClientContext cx, AuthMsg msg) {
    // decode server-first-message
    String s1_msg =
        String.fromCharCodes(Base64Codec.urlSafe().decode(msg.param("data")!));
    Map<String, String> data = _decodeMsg(s1_msg);

    // c2-no-proof
    String cbind_input = _gs2_header;
    String channel_binding =
        Base64Codec.urlSafe().encode(cbind_input.codeUnits);
    String nonce = data["r"]!;
    String c2_no_proof = "c=" + channel_binding + ",r=" + nonce;

    // proof
    String hash = msg.param("hash")!;
    String salt = data["s"]!;
    int iterations = int.parse(data["i"]!);
    String c1_bare = cx.stash["c1_bare"]!;

    String authMsg = c1_bare + "," + s1_msg + "," + c2_no_proof;

    String c2_msg;

    try {
      List<int> saltedPassword = _pbk(hash, cx.pass, salt, iterations);
      String clientProof =
          _createClientProof(hash, saltedPassword, strBytes(authMsg));
      c2_msg = c2_no_proof + ",p=" + clientProof;
    } catch (e) {
      throw AuthException("Failed to compute scram: $e");
    }

    // build auth msg
    Map<String, String> params = Map<String, String>();
    params["data"] = Base64Codec.urlSafe().encode(c2_msg.codeUnits);
    return AuthMsg(name, _injectHandshakeToken(msg, params));
  }

  @override
  void onClientSuccess(AuthClientContext cx, AuthMsg msg) {
    //todo
//    super.onClientSuccess(cx, msg);
  }

  /// Generate a random nonce string
  String _genNonce() {
    String possible =
        "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

    String sb = "";
    for (int i = 0; i < _clientNonceBytes; i++) {
      int index = Random.secure().nextInt(possible.length - 1);
      sb += possible[index];
    }
    String encodedString = sb;
    return encodedString;
  }

  /// If the msg contains a handshake token, inject it into the given params
  static Map _injectHandshakeToken(AuthMsg msg, Map params) {
    String? tok = msg.param("handshakeToken", checked: false);
    if (tok != null) {
      params["handshakeToken"] = tok;
    }
    return params;
  }

  /// Decode a raw scram message.
  static Map<String, String> _decodeMsg(String s) {
    Map<String, String> data = Map<String, String>();
    List<String> toks = s.split(",");
    for (int i = 0; i < toks.length; ++i) {
      String tok = toks[i];
      int n = tok.indexOf('=');
      if (n < 0) continue;
      String key = tok.substring(0, n);
      String val = tok.substring(n + 1);
      data[key] = val;
    }
    return data;
  }

  static List<int> _pbk(
      String hash, String password, String salt, int iterations) {
    String algorithm = "PBKDF2WithHmac" + hash.replaceAll("-", "");
    int keyBytes = _keyBits(hash) ~/ 8;
    List<int> saltBytes = Base64Codec.urlSafe().decode(salt);

    List<int> saltMod = <int>[];
    saltBytes.forEach((val) {
      int newVal = val;
      if (val > 128) {
        newVal = val - 256;
      }
      saltMod.add(newVal);
    });

    List<int> bytes = CryptoUtil.pbk(
        algorithm, strBytes(password), saltMod, iterations, keyBytes);

    return bytes;
  }

  static int _keyBits(String hash) {
    if ("SHA-1" == hash) return 160;
    if ("SHA-256" == hash) return 256;
    if ("SHA-512" == hash) return 512;

    throw ArgumentError("Unsupported hash function: " + hash);
  }

  static String _createClientProof(
      String hash, List<int> saltedPassword, List<int> authMsg) {
    Hash hashHash = hash.toLowerCase() == "sha1" ? sha1 : sha256;
    Hmac hmacSha256 = Hmac(hashHash, saltedPassword); // HMAC-SHA256
    Digest digest = hmacSha256.convert(strBytes("Client Key"));

    List<int> clientKey = digest.bytes;
    clientKey = List.from(clientKey.map<int>((integer) {
      if (integer > 128) {
        return integer - 256;
      } else {
        return integer;
      }
    }));

    List<int> storedKey = sha256.convert(clientKey).bytes;
    storedKey = List.from(storedKey.map<int>((integer) {
      if (integer > 128) {
        return integer - 256;
      } else {
        return integer;
      }
    }));

    Hmac hmacSha256_2 = Hmac(hashHash, storedKey); // HMAC-SHA256
    Digest digest_2 = hmacSha256_2.convert(authMsg);
    List<int> clientSig = digest_2.bytes;
    clientSig = List.from(clientSig.map<int>((integer) {
      if (integer > 128) {
        return integer - 256;
      } else {
        return integer;
      }
    }));

    List<int> clientProof = List<int>.generate(clientKey.length, (i) {
      return (clientKey[i] ^ clientSig[i]);
    }, growable: false);

    //need modify for encoding
    clientProof = List.from(clientProof.map<int>((integer) {
      if (integer < 0) {
        return integer + 256;
      } else {
        return integer;
      }
    }));

    return Base64Codec.urlSafe().encode(clientProof);
  }

  static List<int> strBytes(String s) {
    return utf8.encode(s);
  }

  static final int _clientNonceBytes = 16;
  static final String _gs2_header = "n,,";
}

//////////////////////////////////////////////////////////////////
//// niagara SCRAM (Java)
//////////////////////////////////////////////////////////////////
//
//  /**
//   * Authenticate using Niagara's implementation of
//   * Salted Challenge Response (SCRAM) HTTP Authentication Mechanism
//   *
//   * https://www.ietf.org/archive/id/draft-melnikov-httpbis-scram-auth-01.txt
//   */
//  private void authenticateNiagaraScram(HttpURLConnection c) throws Exception
//  {
//    // authentication uri
//    URI uri = new URI(c.getURL().toString());
//    String authUri = uri.getScheme() + "://" + uri.getAuthority() + "/j_security_check/";
//
//    // nonce
//    byte[] bytes = new byte[16];
//    (new Random()).nextBytes(bytes);
//    String clientNonce = Base64.STANDARD.encodeBytes(bytes);
//
//    c.disconnect();
//    (new NiagaraScram(authUri, clientNonce)).authenticate();
//  }
//
//  class NiagaraScram
//  {
//    NiagaraScram(String authUri, String clientNonce) throws Exception
//    {
//      this.authUri = authUri;
//      this.clientNonce = clientNonce;
//    }
//
//    void authenticate() throws Exception
//    {
//      firstMsg();
//      finalMsg();
//      upgradeInsecureReqs();
//    }
//
//    private void firstMsg() throws Exception
//    {
//      // create first message
//      this.firstMsgBare = "n=" + user + ",r=" + clientNonce;
//
//      // create request content
//      String content = encodePost("sendClientFirstMessage",
//        "clientFirstMessage", "n,," + firstMsgBare);
//
//      // set cookie
//      cookieProperty = new Property("Cookie", "niagara_userid=" + user);
//
//      // post
//      String res = postString(authUri, content, MIME_TYPE);
//
//      // save the resulting sessionId
//      String cookie = cookieProperty.value;
//      int a = cookie.indexOf("JSESSIONID=");
//      int b = cookie.indexOf(";", a);
//      sessionId = (b == -1) ?
//          cookie.substring(a + "JSESSIONID=".length()) :
//          cookie.substring(a + "JSESSIONID=".length(), b);
//
//      // store response
//      this.firstMsgResult = res;
//    }
//
//    private void finalMsg() throws Exception
//    {
//      // parse first msg response
//      Map firstMsg = decodeMsg(firstMsgResult);
//      String nonce = (String) firstMsg.get("r");
//      int iterations = Integer.parseInt((String) firstMsg.get("i"));
//      String salt = (String) firstMsg.get("s");
//
//      // check client nonce
//      if (!clientNonce.equals(nonce.substring(0, clientNonce.length())))
//        throw new CallAuthException("Authentication failed");
//
//      // create salted password
//      byte[] saltedPassword = CryptoUtil.pbk(
//          "PBKDF2WithHmacSHA256",
//          strBytes(pass),
//          Base64.STANDARD.decodeBytes(salt),
//          iterations, 32);
//
//      // create final message
//      String finalMsgWithoutProof = "c=biws,r=" + nonce;
//      String authMsg = firstMsgBare + "," + firstMsgResult + "," + finalMsgWithoutProof;
//      String clientProof = createClientProof(saltedPassword, strBytes(authMsg));
//      String clientFinalMsg = finalMsgWithoutProof + ",p=" + clientProof;
//
//      // create request content
//      String content = encodePost("sendClientFinalMessage",
//        "clientFinalMessage", clientFinalMsg);
//
//      // set cookie
//      cookieProperty = new Property("Cookie",
//          "JSESSIONID=" + sessionId + "; " +
//          "niagara_userid=" + user);
//
//      // post
//      postString(authUri, content, MIME_TYPE);
//    }
//
//    private void upgradeInsecureReqs()
//    {
//      try
//      {
//        URL url = new URL(authUri);
//        HttpURLConnection c = openHttpConnection(url, "GET");
//        try
//        {
//          c.setRequestProperty("Connection", "Close");
//          c.setRequestProperty("Content-Type", "text/plain");
//          c.setRequestProperty("Upgrade-Insecure-Requests", "1");
//          c.setRequestProperty(cookieProperty.key, cookieProperty.value);
//
//          c.connect();
//
//          // check for 302
//          if (c.getResponseCode() != 302)
//            throw new CallHttpException(c.getResponseCode(), c.getResponseMessage());
//
//          // discard response
//          Reader r = new BufferedReader(new InputStreamReader(c.getInputStream(), "UTF-8"));
//          int n;
//          while ((n = r.read()) > 0);
//        }
//        finally
//        {
//          try { c.disconnect(); } catch(Exception e) {}
//        }
//      }
//      catch (Exception e) { throw new CallNetworkException(e); }
//    }
//
//    private String createClientProof(byte[] saltedPassword, byte[] authMsg) throws Exception
//    {
//      byte[] clientKey = CryptoUtil.hmac("SHA-256", strBytes("Client Key"), saltedPassword);
//      byte[] storedKey = MessageDigest.getInstance("SHA-256").digest(clientKey);
//      byte[] clientSig = CryptoUtil.hmac("SHA-256", authMsg, storedKey);
//
//      byte[] clientProof = new byte[clientKey.length];
//      for (int i = 0; i < clientKey.length; i++)
//          clientProof[i] = (byte) (clientKey[i] ^ clientSig[i]);
//
//      return Base64.STANDARD.encodeBytes(clientProof);
//    }
//
//    private Map decodeMsg(String str)
//    {
//      // parse comma-delimited sequence of props formatted "<key>=<value>"
//      Map map = new HashMap();
//      int a = 0;
//      int b = 1;
//      while (b < str.length())
//      {
//        if (str.charAt(b) == ',') {
//          String entry = str.substring(a,b);
//          int n = entry.indexOf("=");
//          map.put(entry.substring(0,n), entry.substring(n+1));
//          a = b+1;
//          b = a+1;
//        }
//        else {
//          b++;
//        }
//      }
//      String entry = str.substring(a);
//      int n = entry.indexOf("=");
//      map.put(entry.substring(0,n), entry.substring(n+1));
//      return map;
//    }
//
//    private String encodePost(String action, String msgKey, String msgVal)
//    {
//      return "action=" + action + "&" + msgKey + "=" + msgVal;
//    }
//
//    private byte[] strBytes(String text) throws Exception
//    {
//      return text.getBytes("UTF-8");
//    }
//
//    private static final String MIME_TYPE = "application/x-niagara-login-support; charset=UTF-8";
//
//    private final String authUri;
//    private final String clientNonce;
//    private String firstMsgBare;
//    private String firstMsgResult;
//    private String sessionId;
//  }
