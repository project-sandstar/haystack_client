///
/// Copyright (c) 2023, BAS Services & Graphics, LLC.
/// Licensed under the Academic Free License version 3.0
///

import 'package:haystack_client/src/auth/auth.dart';
import 'package:haystack_client/src/auth/auth_msg.dart';
import 'package:haystack_client/src/auth/scram_scheme.dart';
import 'package:http/http.dart' as http;


/// AuthScheme is the base class for modeling pluggable authentication algorithms.
abstract class AuthScheme {
////////////////////////////////////////////////////////////////////////////
//// Registry
////////////////////////////////////////////////////////////////////////////

//  /** Convenience for {@link #find(String, boolean) find(name, true)} */
//  public static AuthScheme find(String name) { return AuthScheme.find(name, true); }

  /// Looks up an AuthScheme for the given case-insensitive name.
  static AuthScheme? find(String name, {bool checked = true}) {
    AuthScheme? scheme = _registry[name];
    if (scheme != null) return scheme;
    if (checked) throw ArgumentError("No auth scheme found for '" + name + "'");
    return null;
  }

  static List<AuthScheme> list() {
    return List.from(_registry.values);
  }

  static Map<String, AuthScheme> _registry = _setRegistryMap();
  static Map<String, AuthScheme> _setRegistryMap() {
    Map<String, AuthScheme> m = Map<String, AuthScheme>();
    m["scram"] = ScramScheme();
    //only scram implemented
//    m["hmac"] = new HmacScheme();
//    m["folio2"] = new Folio2Scheme();
//    m["basic"] = new BasicScheme();
    return m;
  }

//
////////////////////////////////////////////////////////////////////////////
//// Construction
////////////////////////////////////////////////////////////////////////////

  AuthScheme(this.name) {
    if (name != name.toLowerCase()) {
      throw ArgumentError("Name must be lowercase: " + name);
    }
  }

////////////////////////////////////////////////////////////////////////////
//// Overrides
////////////////////////////////////////////////////////////////////////////

  /// Scheme name (always normalized to lowercase)
  final String name;

  /// Handle a standardized client authentication challenge message from
  /// the server using RFC 7235.
  ///
  /// @param cx the current {@link AuthClientContext}
  /// @param msg the {@link AuthMsg} sent by the server
  /// @return The {@link AuthMsg} to send back to the server to authenticate
  AuthMsg onClient(AuthClientContext cx, AuthMsg msg);

  /// Callback after successful authentication with the server.
  /// The default implementation is a no-op.
  ///
  /// @param cx the current {@link AuthClientContext}
  /// @param msg the {@link AuthMsg} sent by the server when it authenticated
  ///            the client.
  void onClientSuccess(AuthClientContext cx, AuthMsg msg);

  /// Handle non-standardized client authentication when the standard
  /// process (RFC 7235) fails. If this scheme thinks it can handle the
  /// given response by sniffing the response code and headers, then it
  /// should process and return true.
  ///
  /// @param cx the current {@link AuthClientContext}
  /// @param resp the response message from the server
  /// @param content the body of the response if it had one, or null.
  /// @return true if the scheme processed the response, false otherwise.
  /// Returns false by default.
  bool onClientNonStd(AuthClientContext cx, http.Response resp, String content) {
    return false;
  }
}
