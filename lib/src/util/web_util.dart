///
/// Copyright (c) 2019, BAS Services & Graphics, LLC.
/// Licensed under the Academic Free License version 3.0
///

library haystack.src.webutil;

class WebUtil {
  static bool isToken(String s) {
    if (s == null || s.isEmpty) return false;
    List<int> chars = s.codeUnits;
    bool hasNoTokenChar = chars.any((char) => !isTokenChar(char));
    return !hasNoTokenChar;
  }

  static bool isTokenChar(final int codePoint) {
    return codePoint < 127 && tokenChars[codePoint];
  }

  static List<bool> tokenChars = _setTokenChars();

  static List<bool> _setTokenChars() {
    List<bool> m = List<bool>.generate(127, (i) {
      return i > 32; //i > 0x20
    });

    m['('.codeUnitAt(0)] = false;
    m[')'.codeUnitAt(0)] = false;
    m['<'.codeUnitAt(0)] = false;
    m['>'.codeUnitAt(0)] = false;
    m['@'.codeUnitAt(0)] = false;
    m[','.codeUnitAt(0)] = false;
    m[';'.codeUnitAt(0)] = false;
    m[':'.codeUnitAt(0)] = false;
    m['\\'.codeUnitAt(0)] = false;
    m['"'.codeUnitAt(0)] = false;
    m['/'.codeUnitAt(0)] = false;
    m['['.codeUnitAt(0)] = false;
    m[']'.codeUnitAt(0)] = false;
    m['?'.codeUnitAt(0)] = false;
    m['='.codeUnitAt(0)] = false;
    m['{'.codeUnitAt(0)] = false;
    m['}'.codeUnitAt(0)] = false;
    m[' '.codeUnitAt(0)] = false;
    m['\t'.codeUnitAt(0)] = false;

    return m;
  }
}
