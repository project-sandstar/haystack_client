///
/// Copyright (c) 2019, BAS Services & Graphics, LLC.
/// Licensed under the Academic Free License version 3.0
///

library haystack.src.cryptoutil;

import 'dart:math';

import 'package:crypto/crypto.dart';

/// Crypto utilities.
class CryptoUtil {
  /// Derive a Password-Based Key.
  ///
  /// The only currently supported algorithm is "PBKDF2WithHmacSHA256".
  static List<int> pbk(String algorithm, List<int> password, List<int> salt,
      int iterationCount, int derivedKeyLength)
  {
    if (!(algorithm == "PBKDF2WithHmacSHA256")) {
      throw ArgumentError("Unsupported algorithm: " + algorithm);
    }

    return Pbkdf2.deriveKey(password, salt, iterationCount, derivedKeyLength);
  }
}

/// Pbkdf2
///
/// https://tools.ietf.org/html/rfc2898#section-5.2
/// https://tools.ietf.org/html/rfc5802#section-2.2
///
/// java implementation was from
/// http://stackoverflow.com/questions/9147463/java-pbkdf2-with-hmacsha256-as-the-prf
class Pbkdf2 {
  static List<int> F(List<int> dest, int offset, Hmac hmac, List<int> S, int c,
      int blockIndex)
  {
    int hLen = 32;
    List<int> U_r = List<int>.filled(hLen, 0);

    List<int> U_i = List<int>.generate(S.length, (i) {
      return S[i];
    });

    U_i.addAll([0,0,0,0]);

    for (int i = 0; i < S.length; i++) {
      U_i[i] = S[i];
    }

    U_i = INT(U_i, S.length, blockIndex);

    for (int i = 0; i < c; i++) {
      U_i = hmac.convert(U_i).bytes;
      U_i = List.from(U_i.map<int>((integer) {
        if (integer > 128) {
          return integer - 256;
        } else {
          return integer;
        }
      }));

      U_r = xor(U_r, U_i);
    }

    List<int> result = dest;

    for (int i = 0; i < hLen; i++) {
      result[offset + i] = U_r[i];
    }

    return result;
  }

  static List<int> INT(List<int> dest, int offset, int i) {
    List<int> result = dest;

    result[offset + 0] = i ~/ (256 * 256 * 256);
    result[offset + 1] = i ~/ (256 * 256);
    result[offset + 2] = i ~/ (256);
    result[offset + 3] = i;

    return result;
  }

  static List<int> xor(List<int> dest, List<int> src) {
    List<int> result = dest;

    for (int i = 0; i < dest.length; i++) {
      result[i] ^= src[i];
    }

    return result;
  }

  /// deriveKey
  static List<int> deriveKey(
      List<int> password, List<int> salt, int iterations, int dkLen) {
    Hmac hmacSha256 = Hmac(sha256, password); // HMAC-SHA256

    int hLen = 32; // 20 for SHA1
    int l = max(dkLen, hLen); //  1 for 128bit (16-byte) keys
    int r = dkLen - (l - 1) * hLen; // 16 for 128bit (16-byte) keys
    List<int> T = List<int>.filled(l * hLen, 0);
    int ti_offset = 0;

    for (int i = 1; i <= l; i++) {
      F(T, ti_offset, hmacSha256, salt, iterations, i);
      ti_offset += hLen;
    }

    if (r < hLen) {
      // Incomplete last block
      List<int> DK = List<int>.generate(dkLen, (i) {
        return T[i];
      });

      return DK;
    }
    return T;
  }
}
