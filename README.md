# haystack

Client for SkySpark servers using haystack protocol. 
[Project Haystack]`http://project-haystack.org/` defines a tagging model and REST API for sensor systems such as HVAC, lighting, and energy equipment.
[SkySpark] is a project to serve your IoT data and make analytics.

To use this package you need installed SkySpark server (and apply a some license) or have access to some other SkySpark. 

## Usage

A simple usage example:

    import 'package:haystack_client/haystack_client.dart';
    
    final String uri = "http://localhost:8080/api/demo";
    final String user = "haystack";
    final String pass = "testpass";
    
    void main() async {
      var client = new HSkySparkClient(uri, user, pass);
      await client.open();
      
      var result = client.about();
      print(result);
    }

## Features and bugs

Please file feature requests and bugs at the [issue tracker][tracker].

[tracker]: https://bitbucket.org/kelegorm/haystack_flutter/issues
