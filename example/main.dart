import 'package:haystack_client/haystack_client.dart';

final String uri = "http://localhost/api/demo";
final String user = "su";
final String pass = "su";

void main() async {
  var client = new HSkySparkClient(uri, user, pass);
  await client.open();

  var result = client.about();
  print(result);
}