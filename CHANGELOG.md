# Changelog

## 0.1.0-dev.0.5

  - Added attest key for web using

## 0.1.0-dev.0.4

  - Updated haystack dependency

## 0.1.0-dev.0.3

  - Fixed code style 

## 0.1.0-dev.0.2

  - Fixed readme and about
  - Added example

## 0.1.0-dev.0.1

  - Extracted from haystack package
  
